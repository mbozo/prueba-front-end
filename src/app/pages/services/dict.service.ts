import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICES } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class DictService {

  constructor(public http: HttpClient) { }

  getDict() {
    const url =  URL_SERVICES.DICT;
    console.log('getDict url:: ', url);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(url, httpOptions).pipe (
      map(
        (resp: any) => {
          return resp;
        }
      )
    );
  }
}
