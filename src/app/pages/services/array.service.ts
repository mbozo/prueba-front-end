import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICES } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class ArrayService {

  constructor(public http: HttpClient) { }

  getArray() {
    const url =  URL_SERVICES.ARRAY;
    console.log('getArray url:: ', url);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(url, httpOptions).pipe (
      map(
        (resp: any) => {
          return resp;
        }
      )
    );
  }
}
