import { Component, OnInit } from '@angular/core';
import { DictService } from '../services/dict.service';

@Component({
  selector: 'app-dict',
  templateUrl: './dict.component.html',
  styleUrls: ['./dict.component.css']
})
export class DictComponent implements OnInit {

  // variable encargada de recibir la respuesta del servicio array.
  arrayResponse: any[];
  // variable utilizada para mostrar la respuesta ordenada
  sortedArray: number[];

  // instanciamos arreglo que contiene la cabeera de la tabla
  cabezera_array: String[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y','z'];

  // se declara servicio de dict
  constructor(private dictService: DictService) { }

  ngOnInit() {
  }

  // Metodo llamado desde la vista para cargar la data que se obtenga del servicio Dict.
  obtenerDict() {
    this.dictService.getDict().subscribe((resp: any) => {
      console.log('Dict RESPONSE:: ', resp);
      if (resp.success) {

        // instanciamos variables array vacios.
        this.arrayResponse = [];
        this.sortedArray = [];

        // recorremos la respuesta del servicio.
        resp.data.forEach((res: any) => {
        // se eliminan todos los caracteres que no sean letras.
        let matches = res.paragraph.match(/[a-zA-Z]+/g);
        // validamos que existan letras.
        if (matches !== null) {
          // el arreglo generado por el metodo match es unido y pasado a minusculas.
          matches = matches.join('').toLowerCase();
          // console.log('matches:: ', matches);
          // el string generado por el metodo join es dividido en un arreglo de caracteres.
          const chars = matches.split('');
          // console.log('chars:: ', chars);

          // el arreglo resultante se procede a contar.
            const arrayConteo = this.contarOcurrencias(chars);
            console.log('arrayConteo:: ', arrayConteo);

            // se recorre el arreglo que contiene la cabecera
            for (let i = 0; i < this.cabezera_array.length; i++) {
              const letra = this.cabezera_array[i];
              // encontramos el indice de nuestro arreglo, comprarando la letra con la del arreglo de cabecera.
              const idx = arrayConteo.findIndex(arr => arr.letra === letra);
              // console.log('idx:: ', idx);
              // si no existe el indice, se agrega un nuevo objeto con conteo en 0.
              if (idx === -1) {
                const obj = {
                  letra: letra,
                  quantity: 0
                };
                arrayConteo.push(obj);
              }
              // al finalizar el ciclo, se ordena el arreglo final y se agrega a la variable que mostrara la información en la vista.
              if (i === (this.cabezera_array.length - 1 ) ) {
                this.arrayResponse.push(arrayConteo.sort(this.comparar));
                console.log('this.arrayResponse:: ', this.arrayResponse);
              }
            }
        }
        });
      }
    });
  }

  // método utilizado para contar las occurrencias en el arreglo pasado por parametro.
  contarOcurrencias(original) {
    // se declara objeto de tipo any, inicializado como array vacio.
    const arrayRes: any = [];
    // hace una copia del parametro de entrada.
    const copy = [...original];
    // variable para mostrar el conteo total.
    let total = 0;

    let actual = null;
    // reccorremos todos los elementos del arreglo de entrada.
    for (let i = 0; i < original.length; i++) {
      actual = original[i];
      let conteo = 0;
      // se recorre cada elemento en la copia realizada y se valida que sea igual al original.
      for (let w = 0; w < copy.length; w++) {
        if (actual === copy[w]) {
          // se incrementa la cantidad cuando se encuentra un duplicado.
          conteo++;
          // el elemento de la copia es seteado a undefined
          delete copy[w];
        }
      }
      if (conteo > 0) {
        // construimos un objeto para mostrar en la vista
        const obj = {
          letra: actual,
          quantity: conteo,
        };
        // se realiza el conteo total de la fila.
        total += conteo;
        // cada objeto lo almacenamos en un arreglo.
        arrayRes.push(obj);
      }
    }
    // se incorpora el total como propiedad al objeto.
    arrayRes.total = total;
    return arrayRes;
  }

  // metodo para realizar el ordenamiento, utilizando la propiedad letra.
  comparar(a: any, b: any) {

    const letraA = a.letra;
    const letraB = b.letra;

    let comparison = 0;
    if (letraA > letraB) {
      comparison = 1;
    } else if (letraA < letraB) {
      comparison = -1;
    }
    return comparison;
  }

}
