import { Component, OnInit } from '@angular/core';
import { ArrayService } from '../services/array.service';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  // variable encargada de recibir la respuesta del servicio array.
  arrayResponse: any[];
  // variable utilizada para mostrar la respuesta ordenada
  sortedArray: number[];

  // se declara servicio de array
  constructor(private arrayService: ArrayService) { }

  ngOnInit() {
  }

  // Metodo llamado desde la vista para cargar la data que se obtenga del servicio Array.
  obtenerArray() {
    this.arrayService.getArray().subscribe((resp: any) => {
      console.log('Array RESPONSE:: ', resp);
      if (resp.success) {
        this.contarOcurrencias(resp.data);
      }
    });
  }

  // método utilizado para contar las occurrencias en el arreglo pasado por parametro.
  contarOcurrencias(original) {

    this.arrayResponse = [];
    // hace una copia del parametro de entrada.
    const copy = [...original];

    let actual = null;
    // reccorremos todos los elementos del arreglo de entrada.
    for (let i = 0; i < original.length; i++) {
      actual = original[i];
      let conteo = 0;
      // se recorre cada elemento en la copia realizada y se valida que sea igual al original.
      for (let w = 0; w < copy.length; w++) {
        if (actual === copy[w]) {
          // se incrementa la cantidad cuando se encuentra un duplicado.
          conteo++;
          // el elemento de la copia es seteado a undefined
          delete copy[w];
        }
      }
      if (conteo > 0) {
        // se obtiene la primera posicion del elemento.
        const primeraPosicion = original.findIndex(number => number === actual);

        // se obtiene la primera posicion del elemento desde el arreglo invertido.
        const posicionFinal = [...original].reverse().findIndex(number => number === actual);
        const indice = original.length - 1;
        // se calcula la última posición del elemento dentro del arreglo.
        const ultimaPosicion = posicionFinal >= 0 ? indice - posicionFinal : posicionFinal;
        // construimos un objeto para mostrar en la vista
        const obj = {
          number: actual,
          quantity: conteo,
          firstPosition: primeraPosicion,
          lastPosition: ultimaPosicion
        };
        // cada objeto lo almacenamos en un arreglo que mostrara las filas de la tabla.
        this.arrayResponse.push(obj);
      }
    }
    // ordenamos el arreglo comparando la propiedad numero dentro de los objetos del arreglo.
    const arreglo = [...this.arrayResponse].sort(this.comparar);
    // creamos el arreglo ordenado que solo contenga el numero
    this.sortedArray = arreglo.map(arr => arr.number);
    console.log('arreglo:: ', arreglo);
    console.log('arregloOrdenado:: ', this.sortedArray);
    console.log('this.arrayResponse:: ', this.arrayResponse);
  }

  // metodo para realizar el ordenamiento, utilizando la propiedad number.
  comparar(a: any, b: any) {

    const numeroA = a.number;
    const numeroB = b.number;

    let comparison = 0;
    if (numeroA > numeroB) {
      comparison = 1;
    } else if (numeroA < numeroB) {
      comparison = -1;
    }
    return comparison;
  }


}
