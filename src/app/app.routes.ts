import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArrayComponent } from './pages/array/array.component';
import { DictComponent } from './pages/dict/dict.component';


const app_routes: Routes = [
    {path: 'array-route', component: ArrayComponent},
    {path: 'dict-route', component: DictComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(app_routes, {useHash: true, enableTracing: false})
    ],
    exports: [
        RouterModule
    ]
  })
export class RoutesModule { }
